import { arr } from "./modules/arr.js"
let data = JSON.parse(localStorage.getItem('tasks')) || arr
let containers = {
    containerToday: document.querySelector('.screen__one-today__tasks-container'),
    containerTomorrow: document.querySelector('.screen__one-tomorrow__tasks-container'),
    containerLater: document.querySelector('.screen__one-later__tasks-container')
}
let seperatedTasks = {
    taskToday: [],
    taskTomorrow: [],
    taskLater: []
}
data.map(item => {
    if (item.left == 0) {
        seperatedTasks.taskToday.push(item)
    } else if (item.left == 1) {
        seperatedTasks.taskTomorrow.push(item)
    } else {
        seperatedTasks.taskLater.push(item)
    }
})
function getAmount (arr) {
    let undone = document.querySelector('.undone')
    let amount = 0
    for (let item of arr) {
        if (!item.completed) {
            amount++
        }
    }
    undone.innerHTML = amount
}
getAmount(data)
function reload(tasks, place) {
    place.innerHTML = ''
    let headingTitle = document.createElement('h4')
    for (let task of tasks) {
        // ______________________________Creating..._______________________________
        // ________________Task box________________
        let taskCard = document.createElement('div')
        // _________________Task heading____________
        let taskCardHeading = document.createElement('div')
        let taskStatus = document.createElement('label')
        let taskInp = document.createElement('input')
        let taskTitle = document.createElement('h5')
        // __________________Task content___________
        let taskContent = document.createElement('div')
        let taskDesc = document.createElement('p')
        let taskTimer = document.createElement('span')
        // ________________________________________Decorating..._______________________________
        taskCard.classList.add('task')
        taskCardHeading.classList.add('task-heading')
        taskInp.type = 'checkbox'
        taskInp.checked = task.completed
        if (task.left === 0) {
            headingTitle.innerHTML = `For today → ${tasks.length}`
            taskTimer.innerHTML = 'Today'
        } else if (task.left === 1) {
            headingTitle.innerHTML = `For tomorrow → ${tasks.length}`
            taskTimer.innerHTML = 'Tomorrow'
        } else {
            headingTitle.innerHTML = `Later → ${tasks.length}`
            taskTimer.innerHTML = `${task.left} days left`
        }
        if (task.completed) {
            taskStatus.classList.add('active__checkbox')
            taskStatus.classList.remove('unactive__checkbox')
        } else {
            taskStatus.classList.remove('active__checkbox')
            taskStatus.classList.add('unactive__checkbox')
        }
        taskTitle.innerHTML = task.title
        taskContent.classList.add('task-content')
        taskDesc.innerHTML = `Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quis, recusandae.`
        // ______________________________Appeding..._______________________
        taskCard.append(taskCardHeading, taskContent)
        taskCardHeading.append(taskStatus, taskTitle)
        taskStatus.append(taskInp)
        taskContent.append(taskDesc, taskTimer)
        place.append(headingTitle, taskCard)

        // ____________________Events__________________________
        taskInp.onchange = () => {
            task.completed = !task.completed
            localStorage.setItem('tasks', JSON.stringify(data))
            reload(tasks, place)
            getAmount(data)
        }

    }
}
reload(seperatedTasks.taskToday, containers.containerToday)
reload(seperatedTasks.taskTomorrow, containers.containerTomorrow)
reload(seperatedTasks.taskLater, containers.containerLater)
let pageHome = document.querySelector('.screen__page-home')
let pageSwitchers = document.querySelectorAll('.screen__page-changer')
switcher(pageSwitchers)
let pageHeading = document.querySelector('h1')
function switcher (btns) {
    btns.forEach(btn => {
        btn.onclick = () => {
            btns.forEach(b => {
                b.classList.remove('active')
                b.classList.add('unactive')
            })
            btn.classList.add('active')
            btn.classList.remove('unactive')
            makeNull(containers.containerToday)
            makeNull(containers.containerTomorrow)
            makeNull(containers.containerLater)
            if (pageHome.classList.contains('active')) {
                pageHeading.innerHTML = `All todos`
                reload(seperatedTasks.taskToday, containers.containerToday)   
                reload(seperatedTasks.taskTomorrow, containers.containerTomorrow)   
                reload(seperatedTasks.taskLater, containers.containerLater)   
            } else {
                pageHeading.innerHTML = `Only today`
                reload(seperatedTasks.taskToday, containers.containerToday)   
            }
        }
    })
}
function makeNull (cont) {
    cont.innerHTML = ''
}